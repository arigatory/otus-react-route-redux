import React from 'react';
import { Link } from 'react-router-dom';

export interface IHomePageProps {}

const HomePage: React.FC = (props: IHomePageProps) => {
  return (
    <div>
      <h1>Home Page</h1>
      <ul>
        <li>
          <Link to="/login">Login</Link>
        </li>
        <li>
          <Link to="/register">Register</Link>
        </li>
      </ul>
    </div>
  );
};

export default HomePage;
