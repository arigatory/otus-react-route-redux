import React from 'react';
import { Provider } from 'react-redux';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from './Header';
import AboutPage from '../pages/About';
import HomePage from '../pages/Home';
import LoginPage from '../pages/Login';
import NotFoundPage from '../pages/NotFound';
import RegisterPage from '../pages/Register';
import { store } from '../state';

export interface IApplicationProps {}

const Application: React.FC<IApplicationProps> = (props) => {
  return (
    <Provider store={store}>
      <div className="ui container">
        <BrowserRouter>
          <Header />
          <Routes>
            <Route path="*" element={<NotFoundPage />} />
            <Route path="/" element={<HomePage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
          </Routes>
        </BrowserRouter>
      </div>
    </Provider>
  );
};

export default Application;
