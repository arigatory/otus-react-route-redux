import React, { useEffect } from 'react';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useActions } from '../hooks/useActions';

declare global {
  interface Window {
    gapi: any;
  }
}

const GoogleAuth: React.FC = () => {
  let auth: any;
  const { signIn } = useActions();
  const { signOut } = useActions();
  const { isSignedIn } = useTypedSelector((state) => state.auth);

  useEffect(() => {
    window.gapi.load('client:auth2', () => {
      window.gapi.client
        .init({
          clientId:
            '844675161296-orop4ntnhjkpfk0piqitcm6hna9lpddc.apps.googleusercontent.com',
          scope: 'email',
          plugin_name: 'otus',
        })
        .then(() => {
          auth = window.gapi.auth2.getAuthInstance();
          onAuthChange(auth.isSignedIn.get());
          auth.isSignedIn.listen(onAuthChange);
        });
    });
  }, []);

  const onAuthChange = (isSignedIn: boolean) => {
    if (isSignedIn) {
      signIn();
    } else {
      signOut();
    }
  };

  const onSignInClick = () => {
    window.gapi.auth2.getAuthInstance().signIn();
  };

  const onSignOutClick = () => {
    window.gapi.auth2.getAuthInstance().signOut();
  };

  const renderAuthButton = () => {
    if (isSignedIn === null) {
      return <button className="ui primary loading button">Loading</button>;
    } else if (isSignedIn) {
      return (
        <button onClick={onSignOutClick} className="ui red google button">
          <i className="google icon" />
          Выйти
        </button>
      );
    } else {
      return (
        <button onClick={onSignInClick} className="ui green google button">
          <i className="google icon" />
          Войти с помощью Google
        </button>
      );
    }
  };

  return <div>{renderAuthButton()}</div>;
};

export default GoogleAuth;
